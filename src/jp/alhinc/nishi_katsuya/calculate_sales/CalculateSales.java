package jp.alhinc.nishi_katsuya.calculate_sales;

import java.io.BufferedReader;                                                                                          //import,使えるように教える
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		BufferedReader br = null;
		BufferedReader br2 = null; 
		Map<String, String> branchlist = new HashMap<String, String>();                                                //保持② 箱を作る,key,volue
		Map<String, Long> codesales = new HashMap<String, Long>();                                                     //④ ③で変換したのを入れる箱作る、(String,int)ではエラー、String, Integer

		try {
			try {
				File file = new File(args[0], "branch.lst");
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);                                                                             //null=参照がない、ファイルを入力

			} catch (FileNotFoundException e) {
				System.out.println("支店定義ファイルが存在しません");                                                    //try catchの中にtry catch FileNotFoundExceptionファイルが存在しない
				return;                                                                                                 //クローズとリターンあり、リターンはファイナリーを実行、クローズは実行されない
			}

			String line;
			while ((line = br.readLine()) != null) {                                                                   //ラインを読む、一行ずつデータの読み込み

				String[] codebranch = line.split(",");                                                                 //保持③ 文字を分割する、()の中は区切るやつ,0

				if (!codebranch[0].matches("^\\d{3}$") || codebranch.length != 2 || codebranch[1].isEmpty()) {           //カンマで区切ると要素が増える、bの要素数を見る, 3桁数字で要素数が2以上、支店名b[1]の   
					System.out.println("支店定義ファイルのフォーマットが不正です");                                     //正規表現 3桁数字かbの要素数が2以上かb[1]に改行がある
					return;
				}
				
				codesales.put(codebranch[0], 0L);                                                                        //それぞれ加算する コードが入っているb[0]を利用して、コードと0を先に入れる

				branchlist.put(codebranch[0], codebranch[1]); 
				                                                                                                          //				|| codebranch[1].isEmpty())//保持④ key,volueを入れ			
		   }

			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {                                                         //検索① FilenameFilterを使用

					if (str.matches("[0-9]{8}.rcd")) {                                                                   //検索② 8桁で始まり、rcdで終わる

						return true;
					} else {
						return false;
					}
				}
			};

			File[] list = new File(args[0]).listFiles(filter);
			                                                                                                             //売上げファイルを読み込み①
			int serialnumber = 0;                                                                                      //連番③b1を初期化、以降使えるようブロック外で、スコープ

			for (int i = 0; i < list.length; i++) {

				br2 = new BufferedReader(new FileReader(list[i]));                                                       //売上げファイルを読み込み①,

				String fileName = list[i].getName();                                                                   //連番① ファイルパスからファイル名を取得、8桁とrcdのファイル

				int number8 = Integer.parseInt(fileName.replace(".rcd", ""));                                         //連番② 置換 fileNameの中のrcdを空文字に置換 →aとする int数値に置き換える Integer.parseInt()
				++serialnumber;                                                                                           //読み込むたびにプラス１ずつ
				
				if (i == 0) {                                                                                           //０ 初回の読み込み 上書きされるから 比較する値を作る

					serialnumber = Integer.parseInt(fileName.replace(".rcd", ""));                                         //

				} else if (number8 != (serialnumber)) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				String code = br2.readLine();                                                                           //売り上げファイル読み込み②
				String sales = br2.readLine();                                                                           //行を読み込む、支店コード、売上額
				String three = br2.readLine();

				if (!codesales.containsKey(code)) {                                                                      //containsKey 指定したキーが存在するか
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				if (three != null) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				long intSales = Long.parseLong(sales);                                                               //③sales(売上額)を数値(intSales)に変換 long変換

				String totalfee = Long.toString(intSales);                                                            //合計金額10桁 売上金額を変換 Long

				if (totalfee.length() > 10) {                                                                           //lengthで要素数確認、10を超えれば
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				
				codesales.put(code, codesales.get(code) + intSales);                                                    //それぞれ加算、スコープ ブロック内でしか処理できないので、mapを上へ 0を入れてるので0+volueになる
				
			}
			
			File newfile = new File(args[0], "branch.out");                                                            //ファイル作成① newfileにbranch.out
			newfile.createNewFile();                                                                                     //createNewFileでファイル作成

			FileWriter file1 = new FileWriter(newfile);
			PrintWriter pw = new PrintWriter(new BufferedWriter(file1));                                               //ファイル作成② ファイル出力のためのもの、宣言

			for (Map.Entry<String, String> entry : branchlist.entrySet()) {
				pw.write(entry.getKey() + "," + entry.getValue() + "," + codesales.get(entry.getKey()) + "\r\n");        //ファイルに書き込む +"\r\n"改行    
			}
			
			pw.close();
			
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if (br != null) {
				try {
					br.close();
					br2.close();
				} catch (Exception e) {
					System.out.println("予期せぬエラーが発生しました");                                                 //try catch finally 例外処理、
				}
			}
		}
	}
}
